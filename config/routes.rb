Rails.application.routes.draw do
  resources :payments
  resources :addresses
  resources :customers do
    resources :customer_addresses
  end  
  resources :invoices do
    resources :payments
  end  
  
  devise_for :users, path_prefix: 'auth'
  resources :users
  
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root to: "pages#home"
end
