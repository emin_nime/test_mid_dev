class CreateCustomers < ActiveRecord::Migration[6.0]
  def change
    create_table :customers do |t|
      t.string :surname, null: false, limit: 50
      t.string :name, null: false, limit: 50
      t.string :phone, null: false, limit: 30
      t.string :email, null: false, limit: 30
      t.string :comment

      t.timestamps
    end
  end
end
