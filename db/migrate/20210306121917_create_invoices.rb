class CreateInvoices < ActiveRecord::Migration[6.0]
  def change
    create_table :invoices do |t|
      t.references :customer, index: true
      t.string :num, null: false,  limit: 20
      t.date :date_invoice, null: false
      t.decimal :sum, precision: 10, scale: 2
      t.string :comment, limit: 100

      t.timestamps
    end
  end
end
