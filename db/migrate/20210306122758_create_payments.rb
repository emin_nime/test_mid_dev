class CreatePayments < ActiveRecord::Migration[6.0]
  def change
    create_table :payments do |t|
      t.references :invoice, index: true
      t.string :num, null: false,  limit: 20
      t.date :date_pay, null: false
      t.decimal :sum, null: false, precision: 10, scale: 2
      t.date :period_from
      t.date :period_to
      t.string :comment
      t.datetime :removed_at, default: nil

      t.timestamps
    end
  end
end
