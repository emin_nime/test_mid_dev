class Customer < ApplicationRecord
    has_many :customer_addresses
    has_many :addresses, through: :customer_addresses

    def full_name
        "#{surname} #{name}"
    end    
end
