class CustomerAddress < ApplicationRecord
  belongs_to :customer
  belongs_to :address

  validates :address_id, presence: true
  validates_uniqueness_of :customer_id, scope: [:address_id]
end
