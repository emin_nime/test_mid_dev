class Address < ApplicationRecord
    has_many :customer_addresses, dependent: :delete_all

    validates :name, uniqueness: true
end
