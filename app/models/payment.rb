class Payment < ApplicationRecord
    belongs_to :invoice
    
    validates :num, :date_pay, presence: true
end
