class Invoice < ApplicationRecord
    belongs_to :customer
    has_many :payments, dependent: :delete_all

    validates :num, :date_invoice, presence: true

    def invoice_data
        "Num: #{num}| Date: #{date_invoice}" 
    end    
end
