class CustomerAddressesController < ApplicationController
  before_action :set_customer
  before_action :set_customer_address, only: %i[ show edit update destroy ]

  def new
    @customer_address = @customer.customer_addresses.new
  end

  def edit
  end

  def create
    @customer_address = @customer.customer_addresses.new(customer_address_params)
    if @customer_address.save
      render :create
    else
      render :new
    end
  end

  def update
    respond_to do |format|
      if @customer_address.update(customer_params)
        format.html { redirect_to @customer, notice: "CustomerAddress was successfully updated." }
      else
        format.html { render :edit, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @customer_address.destroy
    respond_to do |format|
      format.html { redirect_to customers_url, notice: "CustomerAddress was successfully destroyed." }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_customer_address
      @customer_address = CustomerAddress.find(params[:id])
    end

    def set_customer
      @customer = Customer.find(params[:customer_id])
    end

    # Only allow a list of trusted parameters through.
    def customer_address_params
      params.require(:customer_address).permit(:customer_id, :address_id)
    end
end
