class ApplicationController < ActionController::Base
    before_action :authenticate_user!
    rescue_from ActiveRecord::RecordNotFound, with: :not_found

    private

    def not_found
        respond_to do |format|
            format.json { head :not_found, content_type: 'application/json' }
            format.html { render file: 'public/404.html', status: 404, layout: false }
            format.js   { head :not_found, content_type: 'application/json' }
        end
    end
end
