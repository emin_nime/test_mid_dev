module ApplicationHelper
    def user_gravatar_url(user, size = 50)
        md5 = Digest::MD5.hexdigest(user.email.strip.downcase)
        "https://www.gravatar.com/avatar/#{md5}?d=identicon&s=#{size}"
    end
end
