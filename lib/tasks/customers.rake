namespace :customers do
  customers = [
    { surname: 'Ivanov', name: 'Ivan', phone: '89562389741', email: 'ivan@user.net' },
    { surname: 'Petry', name: 'Petrov', phone: '89562345789', email: 'petry@user.net' },
    { surname: 'Inna', name: 'Innova', phone: '89562312564', email: 'inna@user.net' },
  ]

  desc 'Create customers'
  task create: :environment do
    puts 'Creating customer:'
    customers.each do |customer|
      Customer.find_or_create_by(surname: customer[:surname], name: customer[:name], phone: customer[:phone]) do |c|
        c.email = customer[:email]
        puts "#{customer[:surname]} - #{customer[:name]} - #{customer[:phone]}"
      end
    end
  end
end
