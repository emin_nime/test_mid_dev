namespace :addresses do
  addresses = [
    { name: '08017 Carrer del Rosari, Barcelona, Spain' },
    { name: '08018 Carrer del Rosari, Barcelona, Spain' },
    { name: '08019 Carrer del Rosari, Barcelona, Spain' },
    { name: '08020 Carrer del Rosari, Barcelona, Spain' },
    { name: '08021 Carrer del Rosari, Barcelona, Spain' }
  ]

  desc 'Create addresses'
  task create: :environment do
    puts 'Creating address:'
    addresses.each do |address|
      Address.find_or_create_by(name: address[:name]) do
        puts address[:name]
      end
    end
  end
end
